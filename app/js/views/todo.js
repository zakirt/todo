define(['jquery', 'underscore', 'backbone', 'handlebars', 'text!templatePath/todo.handlebars'], function($, _, Backbone, Handlebars, todoTemplate) {
	var TodoView = Backbone.View.extend({	
		tagName: 'li',
		className: 'todo-task',

		events: {
			'click .todo-toggle': 'toggleTodo',
			'click .todo-remove': 'removeTodo',
			'dblclick .todo-title': 'editTodo',
			'keypress .todo-editor': 'updateOnEnter',
			'blur .todo-editor': 'updateOnBlur'			
		},

		initialize: function() {			
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			this.listenTo(this.model, 'addTodo', this.editTodo);			
		},
		template: Handlebars.compile(todoTemplate),
		render: function() {
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.toggleClass('todo-done', this.model.get('done'));						
			
			return this;
		},
		toggleTodo: function() {										
			this.model.toggleTodo();									
		},
		removeTodo: function() {
			this.model.destroy();		
		},
		editTodo: function() {	
			this.$el.addClass('todo-edit').find('.todo-editor').focus();	
		},
		saveTodo: function(todoTitle) {
			todoTitle = todoTitle.trim();					
			if (!_.isEmpty(todoTitle)) {								 
				this.$el.removeClass('todo-edit');									
				this.model.save({ title: todoTitle });										
			}
		},
		updateOnEnter: function(e) {
			// Save on ENTER key press	
			if (e.keyCode === 13) {				
				this.saveTodo(e.target.value);
			}
		},
		updateOnBlur: function(e) {
			this.saveTodo(e.target.value);
		}				
	});

	return TodoView;
});