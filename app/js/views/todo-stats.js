define(['jquery', 'underscore', 'backbone', 'handlebars', 'collections/todo-list', 'text!templatePath/footer.handlebars'], function($, _, Backbone, Handlebars, TodoList, footerTemplate) {
	var TodoStatsView = Backbone.View.extend({	
		template: Handlebars.compile(footerTemplate),

		initialize: function() {
			this.listenTo(this.collection, 'add', this.render);
			this.listenTo(this.collection, 'remove', this.render);
			this.listenTo(this.collection, 'change', this.render);
		},
		render: function() {
			this.$el.html(this.template({ numTodos: this.collection.getNumTodos() }));
			return this;
		}
	});

	return TodoStatsView;
});