define(['jquery', 'underscore', 'backbone', 'handlebars', 'views/todo', 'views/todo-stats', 'collections/todo-list'], 
	function($, _, Backbone, Handlebars, TodoView, TodoStatsView, TodoList, detailsTemplate) {
	var TodoListView = Backbone.View.extend({	
		events: {
			'keypress #new-todo': 'createTodo'					
		},
		initialize: function() {					
			this.collection = new TodoList();				
			this.listenTo(this.collection, 'add', this.addOneTodo);
			this.listenTo(this.collection, 'change', this.resetFocus);	
			this.listenTo(this.collection, 'remove', this.resetFocus);				
			this.stats = new TodoStatsView({ el: document.getElementById('todo-footer'), collection: this.collection });
			this.header = $(document.getElementById('todo-header'));
			this.input = $(document.getElementById('new-todo'));
			this.list = $(document.getElementById('todo-list'));		
		},		
		render: function() {	
			this.addAllTodos();		
			this.stats.render();
			this.input.focus();	
		
			return this;
		},
		addOneTodo: function(todoModel) {
			var todoView = new TodoView({ model: todoModel });					
			this.list.append(todoView.render().el);	
		},
		addAllTodos: function() {
			this.collection.forEach(this.addOneTodo, this);			
		},
		focusOnTodoItem: function(id) {
			this.collection.focusOnTodoItem(id);
		},
		createTodo: function(e) {
			// Create a new ToDo model on ENTER
			if (e.keyCode === 13) {
				var todoText = this.input.val().trim();				
				if (!_.isEmpty(todoText)) {
					this.collection.create({ title: todoText });
					this.input.val('');
				}
			}
		},
		resetFocus: function() {
			this.input.focus();
		}		
	});

	return TodoListView;
});