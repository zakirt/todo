define(['underscore', 'backbone'], function(_, Backbone) {
	var Todo = Backbone.Model.extend({
		defaults: {
			title: '',
			done: false
		},
		toggleTodo: function() {
			this.set('done', !this.get('done')).save();	
		}		
	});

	return Todo;
});