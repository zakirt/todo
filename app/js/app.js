define(['jquery', 'underscore', 'router'], function($, _, TodoRouter) {	
	var initialize = function() {		
		var todoApp = new TodoRouter();

		$(function() {			
			todoApp.start();
		});
	};

	return {
		initialize: initialize
	};	
});
