define(['jquery', 'underscore', 'backbone', 'views/todo-list', 'collections/todo-list'], function($, _, Backbone, TodoListView, TodoList) {
	var TodoRouter = Backbone.Router.extend({
		routes: {
			'': 'index',	
			'todos/:id': 'show'
		},
		initialize: function() {			
			this.todoListView = new TodoListView({ el: document.getElementById('todo-app') });
		},
		start: function() {
			Backbone.history.start({ pushState: true });
		},
		index: function() {			
			this.todoListView.collection.fetch();					
		},
		show: function(id) {
			this.todoListView.focusOnTodoItem(id);	
		}
	});

	return TodoRouter;
});
