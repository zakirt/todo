define(['underscore', 'backbone', 'backboneLocalStorage', 'models/todo'], function(_, Backbone, BackboneLocalStorage, Todo) {
	var TodoList = Backbone.Collection.extend({
		model: Todo,
		localStorage: new BackboneLocalStorage('TodoList'),

		// Get the ToDo model specified by id
		focusOnTodoItem: function(id) {
			this.add({ id: id });
			this.get(id).fetch();
		},
		
		// Return the number of incomplete To Do tasks
		getNumTodos: function() {
			return this.where({ done: false }).length;
		}		
	});

	return TodoList;
});