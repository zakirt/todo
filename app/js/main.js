require.config({
	paths: {
		jquery: ['//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min', 'lib/jquery/jquery.min'],
		underscore: ['//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min', 'lib/underscore/underscore'],
		backbone: ['//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min', 'lib/backbone/backbone'],
		backboneLocalStorage: ['//cdnjs.cloudflare.com/ajax/libs/backbone-localstorage.js/1.1.9/backbone.localStorage-min', 'lib/backbone/backbone.localStorage-min'],
		handlebars: ['//cdnjs.cloudflare.com/ajax/libs/handlebars.js/2.0.0-alpha.4/handlebars.min', 'lib/handlebars/handlebars'],
		templatePath: '../templates'
	},
	shim: {
		backbone: {
			deps: ['jquery', 'underscore']
		},
		backboneLocalStorage: {
			deps: ['backbone']
		},
		handlebars: {
			exports: 'Handlebars'
		}
	}
});

require(['app'], function(App) {
	App.initialize();
});