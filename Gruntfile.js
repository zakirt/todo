// Generated on 2014-07-04 using generator-webapp 0.4.9
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Configurable paths
    var config = {
        app: 'app',
        dist: 'dist',
        styles: 'app/styles',
        scripts: 'app/scripts',
        sass: 'app/scripts/sass'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        config: config,

        compass: {
            options: {
                sassDir: 'sass',
                cssDir: 'styles',
                cssPath: '<%= config.app %>/styles/',
                sassPath: '<%= config.app %>/styles/sass'     
            },        
            dist: {
                options: {
                    outputStyle: 'compressed'
                }
            },
            dev: {
                options: {                   
                    outputStyle: 'compact'
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            styles: {
                files: '**/*.scss',
                tasks: ['compass:dev']                
            }
        },
        connect: {
            server: {
                options: {                    
                    base: '<%= config.app %>',
                    hostname: '*',                    
                    livereload: true,                                       
                    port: 9000,
                }
            }
        }
    });   

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['compass:dev']);       
    grunt.registerTask('server', ['connect:server', 'compass', 'watch']);  
};
