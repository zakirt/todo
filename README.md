# README #

### A simple To Do list app created using Backbone.js, RequireJS, and Handlebars.js  ###

* My first application built using Backbone.js and RequireJS technologies. Since I don't have much experience with either technology, I decided to try and apply both in the same project. This app is inspired by the ultra popular ToDo MVC app (https://github.com/tastejs/todomvc/tree/gh-pages/architecture-examples/backbone) though I also added Handlebars.js templates to the mix. There are improvements to be made to the app and I will be updating the project as often as I'll get a chance to work on it.


### Contribution guidelines ###

* Feel free to fork and improve this app.